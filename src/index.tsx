// Dependencies
import * as React from 'react';
import * as ReactDOM from 'react-dom';

// Apis
import { fetchExchangeService } from './apis/fetch-exchange-service/fetch-exchange-service';

// Stores
import { RatesStore } from './stores/rates/rates';

// Components
import { Exchange } from './components/exchange/exchange';

// Styles
import './styles/styles.scss';

const App = () => {
    const pockets = [
        {
            code: 'EUR',
            amount: 500
        },
        {
            code: 'GBP',
            amount: 400
        },
        {
            code: 'USD',
            amount: 300
        }
    ];    
    const store = new RatesStore(pockets, fetchExchangeService);
    
    return <Exchange store={store}></Exchange>
};

ReactDOM.render(
    <App />,
    document.getElementById('root')
);