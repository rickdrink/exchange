import { observable, action, computed, runInAction } from 'mobx';

export type CurrencyRate = {
    code: string
    rate: number
};

export type CurrencyPocket = {
    code: string,
    amount: number
};

export class RatesStore {
    private fetchingService: Function

    @observable currencyCodes: string[] = ['EUR', 'GBP', 'USD']
    @observable ratesData: CurrencyRate[] = []
    @observable isLoading: boolean = true
    @observable sourceCurrency: string = 'EUR'
    @observable destinationCurrency: string = 'GBP'
    @observable sourceAmount: number = undefined
    @observable destinationAmount: number = undefined
    @observable pockets: CurrencyPocket[] = []

    constructor(pockets: CurrencyPocket[], fetchingService: Function, pollingInterval: number = 10000) {
        this.pockets = pockets;
        this.fetchingService = fetchingService;

        this.fetchRates();
        setInterval(() => this.fetchRates(), pollingInterval);
    }

    @computed get exchangeRate(): number {
        if (this.ratesData && this.ratesData.length > 0) {
            const sourceCurrencyRate: number = this.ratesData.find(rate => rate.code === this.sourceCurrency).rate;
            const destinationCurrencyRate: number = this.ratesData.find(rate => rate.code === this.destinationCurrency).rate;
    
            return this.limitDecimals(destinationCurrencyRate / sourceCurrencyRate, 4);
        }
    }

    @computed get sourcePocket(): CurrencyPocket {
        return this.pockets.find(pocket => pocket.code === this.sourceCurrency);
    }

    @computed get destinationPocket(): CurrencyPocket {
        return this.pockets.find(pocket => pocket.code === this.destinationCurrency);
    }

    @computed get isValidSourceAmount(): boolean {
        return this.sourceAmount && this.sourceAmount > 0 && this.sourceAmount <= this.sourcePocket.amount;
    }

    @computed get isValidDestinationAmount(): boolean {
        return this.destinationAmount && this.destinationAmount > 0;
    }

    @computed get isValidExchange(): boolean {
        return this.isValidSourceAmount && this.isValidDestinationAmount && this.sourceCurrency !== this.destinationCurrency;
    }

    @action isValidAmount(category: string = 'source'): boolean {
        return category === 'destination' ? this.isValidDestinationAmount : this.isValidSourceAmount;
    }

    @action
    setSourceCurrency(code: string) {
        if (code) {
            this.sourceCurrency = code;

            if (!!this.sourceAmount) {
                this.setExchangeAmount(this.sourceAmount, 'source');
            }
        }
    }

    @action
    setDestinationCurrency(code: string) {
        if (code) {
            this.destinationCurrency = code;

            if (!!this.destinationAmount) {
                this.setExchangeAmount(this.destinationAmount, 'destination');
            }
        }
    }

    @action
    setExchangeAmount(input: number, category: string = 'source') {
        if (input <= 0) {
            this.destinationAmount = 0;
            this.sourceAmount = 0;
        } else if (category === 'destination') {
            this.destinationAmount = this.limitDecimals(input);
            this.sourceAmount = this.limitDecimals(input / this.exchangeRate);
        } else {
            this.sourceAmount = this.limitDecimals(input);
            this.destinationAmount = this.limitDecimals(input * this.exchangeRate);
        }
    }

    @action
    exchangeCurrency() {
        this.sourcePocket.amount -= this.sourceAmount;
        this.destinationPocket.amount += this.destinationAmount;
        this.sourceAmount = undefined;
        this.destinationAmount = undefined;
    }

    @action
    async fetchRates() {
        const data: any = await this.fetchingService();
        
        runInAction(() => {
            this.isLoading = false;
            
            if (data && data.rates) {
                this.ratesData = this.currencyCodes.map(code => ({code, rate: data.rates[code]}));
            }
        });
    }

    @action
    limitDecimals(input: number, decimals: number = 2): number {
        if (input < 0) {
            return;
        }

        return parseFloat(input.toFixed(decimals));
    }

    @action
    formatCurrency(currency: string, amount: number, languageCode: string = 'en-GB') {
        if (!currency && amount <= 0) {
            return;
        }
        
        return new Intl.NumberFormat('en-GB', {style: 'currency', currency}).format(amount);
    }
};