import { exchangeServiceUrl } from '../../config';

export async function fetchExchangeService(url: string = exchangeServiceUrl): Promise<any> {
    try {
        const response: Response = await fetch(url);
        const data: Promise<any> = await response.json();

        return data;
    } catch(error) {
        console.error(error);
    }
}