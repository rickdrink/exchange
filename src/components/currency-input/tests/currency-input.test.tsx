import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { CurrencyInput } from '../currency-input';
import { RatesStore } from '../../../stores/rates/rates';
import { openexchangeSampleData } from '../../../stores/rates/tests/fixtures/openexchange-sample-data';
import { pocketsSampleData } from '../../../stores/rates/tests/fixtures/pockets-sample-data';

describe('Currency Input', () => {
    const store: RatesStore = new RatesStore(pocketsSampleData, () => openexchangeSampleData);
    const sourceWrapper: ShallowWrapper<CurrencyInput> = shallow(<CurrencyInput category="source" store={store} />);
    const destinationWrapper: ShallowWrapper<CurrencyInput> = shallow(<CurrencyInput category="destination" store={store} />);
    
    it('should render', () => {
        expect(sourceWrapper.exists()).toBeTruthy();
    });

    it('should have a category', () => {
        const instance: CurrencyInput = sourceWrapper.instance() as CurrencyInput;

        expect(instance.props.category).toBe('source');
    });

    it('should have a store', () => {
        const instance: CurrencyInput = sourceWrapper.instance() as CurrencyInput;

        expect(instance.props.store).toBeDefined();
    });

    it('should have a number input', () => {
        expect(sourceWrapper.find('input[type="number"]').exists()).toBeTruthy();
        expect(destinationWrapper.find('input[type="number"]').exists()).toBeTruthy();
    });

    it('should have a helping text', () => {        
        expect(destinationWrapper.find('.help').exists()).toBeTruthy();
    });
});