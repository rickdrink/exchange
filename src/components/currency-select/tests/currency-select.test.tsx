import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { CurrencySelect } from '../currency-select';
import { RatesStore } from '../../../stores/rates/rates';
import { openexchangeSampleData } from '../../../stores/rates/tests/fixtures/openexchange-sample-data';
import { pocketsSampleData } from '../../../stores/rates/tests/fixtures/pockets-sample-data';

describe('Currency Select', () => {
    const store: RatesStore = new RatesStore(pocketsSampleData, () => openexchangeSampleData);
    const wrapper: ShallowWrapper<CurrencySelect> = shallow(<CurrencySelect category="source" store={store} />);
    
    it('should render', () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it('should have a category', () => {
        const instance: CurrencySelect = wrapper.instance() as CurrencySelect;

        expect(instance.props.category).toBe('source');
    });

    it('should have a store', () => {
        const instance: CurrencySelect = wrapper.instance() as CurrencySelect;

        expect(instance.props.store).toBeDefined();
    });

    it('should have a select', () => {
        expect(wrapper.find('select').exists()).toBeTruthy();
    });

    it('should have a helping text', () => {        
        expect(wrapper.find('.help').exists()).toBeTruthy();
    });
});