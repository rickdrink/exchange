import * as React from 'react';
import { observer } from 'mobx-react';
import { RatesStore } from '../../stores/rates/rates';
import { CurrencySelect } from '../currency-select/currency-select';

interface ICurrencyInputProps {
    category: string,
    store: RatesStore
}

@observer
export class CurrencyInput extends React.Component<ICurrencyInputProps, {}> {
    getHelpText(): string {
        let result: string = '';
        
        if (this.props.category === 'destination' && this.props.store.exchangeRate > 0) {
            const sourceCurrencyText: string = this.props.store.formatCurrency(this.props.store.sourceCurrency, 1);
            const destinationCurrencyText: string = this.props.store.formatCurrency(this.props.store.destinationCurrency, this.props.store.exchangeRate);
            result = `${sourceCurrencyText} = ${destinationCurrencyText}`;
        }

        return result;
    }

    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        const currentValue: string = e.currentTarget.value;
        let amount: number = currentValue === '' ? 0 : parseFloat(currentValue);

        if (currentValue.match(/\.\d{3}/)) {
            amount = Math.floor(amount * 100) / 100;
        }
        
        this.props.store.setExchangeAmount(amount, this.props.category);
    }

    render() {
        const inputValue: number | string = (this.props.category === 'destination' ? this.props.store.destinationAmount : this.props.store.sourceAmount) || '';
        const inputClassName : string = !this.props.store.isValidAmount(this.props.category) && inputValue !== '' ? 'input is-danger' : 'input';

        return (
            <div className="field is-grouped">
                <CurrencySelect category={this.props.category} store={this.props.store} />
                <div className="control is-expanded">
                    <input className={inputClassName} type="number" value={inputValue} onChange={e => this.onChange(e)} />
                    <p className="help has-text-right">{this.getHelpText()}</p>
                </div>
            </div>
        );
    }
}