import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { Exchange } from '../exchange';
import { RatesStore } from '../../../stores/rates/rates';
import { openexchangeSampleData } from '../../../stores/rates/tests/fixtures/openexchange-sample-data';
import { pocketsSampleData } from '../../../stores/rates/tests/fixtures/pockets-sample-data';

describe('Exchange', () => {
    const store: RatesStore = new RatesStore(pocketsSampleData, () => openexchangeSampleData);
    const wrapper: ShallowWrapper<Exchange> = shallow(<Exchange store={store} />);
    
    it('should render', () => {
        expect(wrapper.exists()).toBeTruthy();
    });

    it('should have store', () => {
        const instance: Exchange = wrapper.instance() as Exchange;

        expect(instance.props.store).toBeDefined();
    });

    it('should have disabled exchange button', () => {
        expect(wrapper.find('button').prop('disabled')).toBeTruthy();
    });

    it('should have enabled exchange button', () => {
        store.setSourceCurrency('USD');
        store.setDestinationCurrency('GBP');
        store.setExchangeAmount(10, 'source');

        expect(wrapper.find('button').prop('disabled')).toBeFalsy();
    });

    it('should click button to exchange currency', () => {
        store.setSourceCurrency('USD');
        store.setDestinationCurrency('GBP');
        store.setExchangeAmount(10, 'source');
        wrapper.find('button').simulate('click');

        expect(store.sourcePocket.amount).toBe(290);
        expect(store.destinationPocket.amount).toBe(407.54);
    });
});