import * as React from 'react';
import { observer } from 'mobx-react';
import { RatesStore } from '../../stores/rates/rates';
import { CurrencyInput } from '../currency-input/currency-input';

interface IExchangeProps {
    store: RatesStore
}

@observer
export class Exchange extends React.Component<IExchangeProps, {}> {
    onClick() {
        this.props.store.exchangeCurrency();
    }
    
    render() {
        const exchangeButton: JSX.Element = (
            <div className="field is-grouped is-grouped-centered">
                <div className="control">
                    <button className="button is-primary" onClick={e => this.onClick()} disabled={!this.props.store.isValidExchange}>Exchange</button>
                </div>
            </div>
        );

        return (
            <div className="box">
                <div className="media">
                    <CurrencyInput category="source" store={this.props.store} />
                    <CurrencyInput category="destination" store={this.props.store} />
                    {exchangeButton}
                </div>
            </div>
        );
    }
}