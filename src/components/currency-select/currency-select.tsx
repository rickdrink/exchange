import * as React from 'react';
import { observer } from 'mobx-react';
import { RatesStore } from '../../stores/rates/rates';

interface ICurrencySelectProps {
    category: string,
    store: RatesStore
}

@observer
export class CurrencySelect extends React.Component<ICurrencySelectProps, {}> {
    generateOptions(): JSX.Element[] {
        return this.props.store.pockets.map(pocket => <option key={pocket.code} value={pocket.code}>{pocket.code}</option>);
    }

    getSelectedCurrency(): string {
        return this.props.category === 'destination' ? this.props.store.destinationCurrency : this.props.store.sourceCurrency;
    }

    getPocketBalance(): string {
        const amount = this.props.store.pockets.find(pocket => pocket.code === this.getSelectedCurrency()).amount;
        return this.props.store.formatCurrency(this.getSelectedCurrency(), amount);
    }

    onChange(e: React.ChangeEvent<HTMLSelectElement>) {
        if (this.props.category === 'destination') {
            this.props.store.setDestinationCurrency(e.currentTarget.value);
        } else {
            this.props.store.setSourceCurrency(e.currentTarget.value);
        }
    }

    render() {
        const inputValue: number | string = (this.props.category === 'destination' ? this.props.store.destinationAmount : this.props.store.sourceAmount) || '';
        const selectClassName: string = this.props.store.sourceCurrency === this.props.store.destinationCurrency ? 'select is-danger' : 'select';
        const helpClassName: string = !this.props.store.isValidAmount(this.props.category) && inputValue !== '' ? 'help is-danger' : 'help';
        
        return (
            <div className="control">
                <div className={selectClassName}>
                    <select value={this.getSelectedCurrency()} onChange={e => this.onChange(e)}>
                        {this.generateOptions()}
                    </select>
                </div>
                <p className={helpClassName}>Balance {this.getPocketBalance()}</p>
            </div>
        );
    }
}