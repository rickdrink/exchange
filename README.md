# Exchange
Revolut-like exchange widget

## Objective

Implement functionality of [this screen](https://www.youtube.com/watch?v=c0zPSiKYipc&feature=youtu.be&t=29s) in a custom web widget using FX
rates from a source such as [Open Exchange Rates](https://openexchangerates.org/).

## Tech stack

This projects has been developed using:

- Node.js
- NPM
- React
- Typescript
- MobX
- Bulma
- Jest
- Enzyme
- Webpack

## Installation

Execute in a terminal `npm install`.

## Commands

The following commands are available via terminal:

- Run build using `npm run build`. 
- Run build in watch mode using `npm run watch`.
- Run development server using `npm run serve`.
- Run test in watch mode using `npm run test`.
- Run build/test/dev-server at the same time using `npm start`.

## Author

Riccardo Bevilacqua (bevilacqua.riccardo@gmail.com)

## MIT License

Copyright (c) 2019 Riccardo Bevilacqua

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
