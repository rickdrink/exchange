export const pocketsSampleData = [
    {
        code: 'EUR',
        amount: 500
    },
    {
        code: 'GBP',
        amount: 400
    },
    {
        code: 'USD',
        amount: 300
    }
];