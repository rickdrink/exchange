import { RatesStore } from '../rates';
import { openexchangeSampleData } from './fixtures/openexchange-sample-data';
import { pocketsSampleData } from './fixtures/pockets-sample-data';

describe('Rates Store', () => {
    const store = new RatesStore(pocketsSampleData, () => openexchangeSampleData);

    it('should have a default source currency', () => {
        expect(store.sourceCurrency).toBeDefined();
    });

    it('should have a default destination currency', () => {
        expect(store.destinationCurrency).toBeDefined();
    });

    it('should have at least 3 currency pockets', () => {
        expect(store.pockets).toBeDefined();
        expect(store.pockets.length).toBeGreaterThanOrEqual(3);
    });

    it('should set source currency', () => {
        store.setSourceCurrency('USD');
        expect(store.sourceCurrency).toBe('USD');
    });

    it('should set destination currency', () => {
        store.setDestinationCurrency('EUR');
        expect(store.destinationCurrency).toBe('EUR');
    });

    it('should have exchange rate', () => {
        store.setSourceCurrency('USD');
        store.setDestinationCurrency('GBP');
        expect(store.exchangeRate).toBe(0.7537);
    });

    it('should have a valid source amount', () => {
        store.sourceAmount = 10;
        expect(store.isValidSourceAmount).toBeTruthy();
    });

    it('should have an invalid source amount', () => {
        store.sourceAmount = 100000;
        expect(store.isValidSourceAmount).toBeFalsy();
    });

    it('should have a valid destination amount', () => {
        store.destinationAmount = 10;
        expect(store.isValidDestinationAmount).toBeTruthy();
    });

    it('should have an invalid destination amount', () => {
        store.destinationAmount = 100000;
        expect(store.isValidDestinationAmount).toBeTruthy();
    });

    it('should set source amount', () => {
        store.setExchangeAmount(50, 'source');
        expect(store.sourceAmount).toBe(50);
    });

    it('should set destination amount', () => {
        store.setExchangeAmount(50.5168, 'destination');
        expect(store.destinationAmount).toBe(50.52);
    });

    it('should exchange currency', () => {
        store.setSourceCurrency('USD');
        store.setDestinationCurrency('GBP');
        store.setExchangeAmount(10, 'source');
        store.exchangeCurrency();

        expect(store.sourcePocket.amount).toBe(290);
        expect(store.destinationPocket.amount).toBe(407.54);
    });

    it('should limit decimals', () => {
        expect(store.limitDecimals(2.451325)).toBe(2.45);
        expect(store.limitDecimals(2.456325)).toBe(2.46);
    });

    it('should format currency', () => {
        expect(store.formatCurrency('USD', 100)).toBe('$100.00');
    });
});